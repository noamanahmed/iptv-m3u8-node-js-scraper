const dgram = require('dgram');
const express = require('express');
const helmet = require('helmet');
const http = require('http');

const app = express();
app.use(helmet());

const server = http.createServer(app);
const udpServer = dgram.createSocket('udp4');

let streamClients = new Set();

udpServer.on('message', (msg, rinfo) => {
  
  let format;
  if (msg[0] === 0x47 && msg[188] === 0x47) {
    format = 'ts';
  } else if (msg[0] === 0x00 && msg[1] === 0x00 && msg[2] === 0x01) {
    format = 'mpg';
  }else if (msg[4] === 0x66 && msg[5] === 0x74 && msg[6] === 0x79 && msg[7] === 0x70) {
    const ftyp = msg.subarray(4, 8).reduce((a, b) => a + String.fromCharCode(b), '');
    if (ftyp === 'msdh') {
      format = 'hevc';
    } else if (ftyp === 'mp42' || ftyp === 'isom') {
      format = 'mp4';
    } else {
      format = 'unknown';
    }
  }else {
    format = 'unknown';
  }

  let contentType;
  if (format === 'ts') {
    contentType = 'video/mp2t';
  } else if (format === 'mpg') {
    contentType = 'video/mpeg';
  } else if (format === 'hevc') {
    contentType =  'video/mp4';
  } else if (format === 'mp4') {
    contentType =  'video/mp4';
  } else {
    contentType = 'video/mp2t';
    // console.error('Unknown stream format');
    return;
  }

  streamClients.forEach((res) => {
    if (!res.headersSent) {
      res.writeHead(200, {
        'Content-Type': contentType,
        'Transfer-Encoding': 'chunked',
        'Cache-Control': 'no-cache, no-store',
        'Pragma': 'no-cache',
        'Expires': 0,
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'X-Requested-With',
      });
    }
    res.write(msg);
  });
});

app.get('/stream', (req, res) => {
  // TODO: add authentication logic here
  streamClients.add(res);

  req.on('close', () => {
    streamClients.delete(res);
  });
});

server.listen(8080, () => {
  console.log(`Server listening on port ${server.address().port}`);
  udpServer.bind(1234);
});
