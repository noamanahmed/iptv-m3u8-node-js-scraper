const fs = require("fs");
const parser = require('iptv-playlist-parser')
const { execSync } = require("child_process");
var url = require("url");
var path = require("path");
require('dotenv').config()


const m3u8Contents = fs.readFileSync(process.env.M3U8_FILE,"utf8");
const result = parser.parse(m3u8Contents)
const userAgent= process.env.USERAGENT || 'VLC/3.0.16 LibVLC/3.0.16'
const speedLimit = process.env.SPEEDLIMIT || '2000K'
const enableDebuggingLogs = process.env.DEBUG || false
let tmpDownloadFilePath = process.env.TEMP_DOWNLOAD_DIR || 'downloads-tmp/'
tmpDownloadFilePath = tmpDownloadFilePath.endsWith('/') ? tmpDownloadFilePath.slice(0, -1) : tmpDownloadFilePath;

let downloadFilePath = process.env.DOWNLOAD_DIR || 'downloads'
downloadFilePath = downloadFilePath.endsWith('/') ? downloadFilePath.slice(0, -1) : downloadFilePath;

let completedDownloadFilePath = process.env.COMPLETED_DOWNLOAD_DIR || 'completed-downloads'
completedDownloadFilePath = completedDownloadFilePath.endsWith('/') ? completedDownloadFilePath.slice(0, -1) : completedDownloadFilePath;


if (! fs.existsSync(tmpDownloadFilePath))
{
    console.error("tmpDownloadFilePath not set properly")
    return;
}


if (! fs.existsSync(downloadFilePath))
{
    console.error("downloadFilePath not set properly")
    return;
}

if (! fs.existsSync(completedDownloadFilePath))
{
    console.error("completedDownloadFilePath not set properly")
    return;
}

console.log("Removing temporary files")
fs.readdirSync(tmpDownloadFilePath).forEach(f => fs.rmSync(`${tmpDownloadFilePath}/${f}`));


console.log('===============')
console.log('Total Files Count : ' + result.items.length)
console.log('===============')
result.items.forEach((element,index) => {
    console.log('===============')
    console.log('Attempting #' +index+ ' File')
    let pathName = url.parse(element.url).pathname;
    let fileName = element.name + '.' + path.basename(pathName).split('.').pop()
    fileName = fileName.replace(/\|/g, ",")
    let singleFilePath = tmpDownloadFilePath + '/' + fileName
    let singleActualFilePath = downloadFilePath + '/' + fileName
    let singleCompletedFilePath = completedDownloadFilePath + '/' + fileName
    
    if (fs.existsSync(singleCompletedFilePath))
    {
        console.warn("Skipping: The file lock already exists at " +singleCompletedFilePath + ' so it should be present at ' + singleActualFilePath)
        return;
    }

    try {
        let cmd = 'wget --no-verbose --user-agent="'+userAgent+'"  --limit-rate='+speedLimit+' ' +element.url +' -O "'+ singleFilePath+'" &> /dev/null && '+'touch "' + singleCompletedFilePath + '"';
        console.log("Running Command")
        console.log(cmd)   
        execSync(cmd, (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            if(enableDebuggingLogs)
            {
                console.log(`stdout: ${stdout}`);
            }
        });
        console.log("Moving File Command")
        cmd = 'mv "' + singleFilePath+ '" "' + singleActualFilePath + '"'
        console.log(cmd)
        execSync(cmd, (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            if(enableDebuggingLogs)
            {
                console.log(`stdout: ${stdout}`);
            }
            
        });
    } catch (error) {
        console.warn("Exception " + error.message)            
    }

    return;
});

process.on('SIGINT', () => {
    this.child.kill('SIGINT')
})