#!/bin/bash
path=$( cat .env  | grep ^DOWNLOAD_DIR= | tail -1 | sed 's/DOWNLOAD_DIR=//' | sed 's/\/$//')
rclone_mount=$( cat .env  | grep ^RCLONE_MOUNT= | tail -1 | sed 's/RCLONE_MOUNT=//' | sed 's/\/$//')
rclone_mount_path=$( cat .env  | grep ^RCLONE_PATH= | tail -1 | sed 's/RCLONE_PATH=//' | sed 's/\/$//')
rclone_bw_limit=$( cat .env  | grep ^RCLONE_BW_LIMIT= | tail -1 | sed 's/RCLONE_BW_LIMIT=//' | sed 's/\/$//')

echo "Path is $path"

while true
do
    rclone move $path $rclone_mount:$rclone_mount_path/ -P -vv --bwlimit=$rclone_bw_limit --no-traverse 
	sleep 30
done